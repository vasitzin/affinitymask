@echo off

set /a cores = %NUMBER_OF_PROCESSORS% - 2
mingw32-make -j %cores% -sC .\build