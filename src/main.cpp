#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#ifndef TITLE
#define TITLE "undefined"
#endif
#ifndef VERSION
#define VERSION "0"
#endif

std::map<size_t, char> hex;

/**
 * Build a map of hexadecimal symbols.
 */
void build_hex_chars() {
  hex[0] = '0';
  hex[1] = '1';
  hex[2] = '2';
  hex[3] = '3';
  hex[4] = '4';
  hex[5] = '5';
  hex[6] = '6';
  hex[7] = '7';
  hex[8] = '8';
  hex[9] = '9';
  hex[10] = 'A';
  hex[11] = 'B';
  hex[12] = 'C';
  hex[13] = 'D';
  hex[14] = 'E';
  hex[15] = 'F';
}

// Print error char messages.
void print_error(const char *msg) { std::cerr << msg << std::endl; }

// Print error string messages.
void print_error(std::string &msg) { std::cerr << msg << std::endl; }

/**
 * Get the max of cores vector.
 */
size_t get_max(std::vector<size_t> &cores, size_t &coresz) {
  size_t max{cores[0]};
  for (size_t i = 1; i < coresz; ++i)
    if (cores[i] > max) max = cores[i];
  return max;
}

/**
 * Parse cmd options string, get value.
 */
std::string get_flag(int argc, char *argv[], const std::string &soption, const std::string &loption,
                     const char delimeter) {
  std::string fn{"get_flag"};
  std::string cmd{};
  std::string arg{};
  size_t delipos{};
  for (int i = 0; i < argc; ++i) {
    arg = argv[i];
    if (arg.find(soption) != std::string::npos || arg.find(loption) != std::string::npos) {
      delipos = arg.find_first_of(delimeter);
      if (delipos == std::string::npos)
        throw "[Error:" + fn + "] Equal '=' sign required on flag '" + arg +
            "'. Eg -f=value or --flag=value.";
      else if (delipos == arg.length() - 1)
        throw "[Error:" + fn +
            "] No value after equal '=' sign found. Eg -f=value or --flag=value.";
      cmd = arg.substr(delipos + 1);
      return cmd;
    }
  }
  throw "[Error:" + fn + "] A required flag " + soption + "/" + loption + " wasn't found!";
}

/**
 * Parse cmd options string, get bool.
 */
bool is_flag(int argc, char *argv[], const std::string &soption, const std::string &loption) {
  std::string arg{};
  for (int i = 0; i < argc; ++i) {
    arg = argv[i];
    if (arg == soption || arg == loption) return true;
  }
  return false;
}

/**
 * Split the option string on delimeter.
 */
std::vector<std::string> split(std::string &option, char delimeter) {
  std::replace(option.begin(), option.end(), delimeter, ' ');
  std::istringstream stream(option);
  std::vector<std::string> res{std::istream_iterator<std::string>(stream),
                               std::istream_iterator<std::string>()};
  return res;
}

/**
 * Expand number ranges of N-M format.
 */
std::vector<size_t> expand(std::vector<std::string> &splits) {
  std::string fn{"expand"};
  std::vector<size_t> cores{};
  size_t start{};
  size_t stop{};
  size_t alone{};
  size_t found{};
  for (std::string str : splits) {
    found = str.find_first_of("-");
    try {
      if (found != std::string::npos) {
        start = stoi(str.substr(0, found));
        stop = stoi(str.substr(found + 1)) + 1;
        for (size_t i = start; i < stop; ++i) cores.push_back(i);
      } else {
        alone = stoi(str);
        cores.push_back(alone);
      }
    } catch (const std::invalid_argument &err) {
      throw "[Error:" + fn + "] On '" + str + "' invalid argument error!";
    }
  }
  return cores;
}

/**
 * Remove duplicate core values from vector.
 */
std::vector<size_t> uniq_codes(const std::vector<size_t> &cores, const size_t &coresz) {
  std::vector<size_t> new_cores{};
  std::set<size_t> set_tmp{};
  for (size_t i = 0; i < coresz; ++i) set_tmp.insert(cores[i]);
  new_cores.assign(set_tmp.begin(), set_tmp.end());
  return new_cores;
}

/**
 * Calculate the codes for the max core specified.
 */
std::vector<uint64_t> calc_codes(const size_t &max) {
  std::vector<uint64_t> codes{};
  uint64_t prev{1};
  codes.push_back(prev);
  for (size_t i = 0; i < max; ++i) {
    prev = 2 * prev;
    codes.push_back(prev);
  }
  return codes;
}

/**
 * Convert a decimal integer to hexadecimal string.
 */
std::string int_to_hex(const uint64_t &mask) {
  std::string mask_str{};
  uint64_t q{mask};
  size_t r{};
  while (q > 15) {
    size_t q_old{q};
    q = q / 16;
    r = q_old % 16;
    mask_str = hex[r] + mask_str;
  }
  if (q != 0) mask_str = hex[q] + mask_str;
  return mask_str;
}

int main(int argc, char **argv) {
  try {
    if (is_flag(argc, argv, "-h", "--help")) {
      std::cout << (TITLE + (std::string) " v" + VERSION) << std::endl;
      std::cout << std::endl;
      std::cout << "-m=/--mask= [RANGE]\t"
                << "the range of nums representing your affinity mask (eg 0-3,6)" << std::endl;
      std::cout << "-t/--threads\t\t"
                << "return number of threads in output eg 'maskitty -m=0-5 -t' returns '6 3F'"
                << std::endl;
      std::cout << "-h/--help\t\t"
                << "print this menu" << std::endl;
      return 0;
    }

    build_hex_chars();             // build the hex symbols map
    std::vector<size_t> cores{};   // the cores selected
    std::size_t coresz{};          // sum of the selected cores
    std::vector<uint64_t> codes{}; // integers asigned for each core
    std::string mask{};            // the final mask in hexadecimal

    std::vector<std::string> mask_opt_split{};
    {
      std::string mask_opt{get_flag(argc, argv, "-m", "--mask", '=')};
      mask_opt_split = split(mask_opt, ',');
    }
    std::vector<size_t> cores_tmp = expand(mask_opt_split);
    std::size_t coresz_tmp = cores_tmp.size();
    cores = uniq_codes(cores_tmp, coresz_tmp);
    coresz = cores.size();
    codes = calc_codes(get_max(cores, coresz));

    {
      uint64_t mask_dec{};                               // the mask in decimal
      for (size_t core : cores) mask_dec += codes[core]; // calculate the mask
      mask = int_to_hex(mask_dec);                       // convert to hexadecimal
    }

    // output hex result with sum of cores if specified
    if (is_flag(argc, argv, "-t", "--threads")) std::cout << coresz << " ";
    std::cout << mask << std::endl;
  } catch (const char *err) {
    print_error(err);
    return 2;
  } catch (std::string err) {
    print_error(err);
    return 2;
  } catch (...) {
    print_error("Unexpected error detected!");
    return 1;
  }
  return 0;
}
