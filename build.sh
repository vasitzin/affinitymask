#!/bin/bash

cores=$(($(lscpu | awk '/^CPU\(s\):/ {printf $2}')-2))
make -j $cores -sC ./build
