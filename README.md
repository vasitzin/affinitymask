## What is this

This is a utility written in C++ that calculates the affinity mask for a cpu core range.
Binaries are provided in the [Releases](https://gitlab.com/vasitzin/maskitty/-/releases) section.

## How to use it

You'll need to pass the `-m` flag to provide the core range eg `0-3,6`,
this will return the affinity mask for cores `0, 1, 2, 3 and 6` which is `4F`.

```
~❱ maskitty -m=0-3,6
4F 
```

There's also a help menu

```
~❱ maskitty -h
maskitty v1.0.0

-m=/--mask= [RANGE]  the range of nums representing your affinity mask (eg 0-3,6)
-t/--threads         return number of threads in output eg 'maskitty -m=0-5 -t' returns '6 3F'
-h/--help            print this menu
```

## Compiling

Since this is a small one file project it's easy to compile it with your preferred method.

Otherwise...

#### Compile with provided scripts

You will need `make`, `cmake` and `gcc` to be installed on your system,
then use `./build.sh` or `.\build.bat` and you should get the following structure:

```
.
└── build
    └── bin
        └── maskitty
```
